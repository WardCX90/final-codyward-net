var gulp = require('gulp');
var sass = require('gulp-sass');
var prefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync').create();

//Minimizes .js files
gulp.task('scripts', function(){
    gulp.src('app/js/**/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({stream: true}));
});

// gulp.task('styles', function(){
//     gulp.src('app/scss/**/*.scss')
//         .pipe(plumber())
//         .pipe(sass({
//             style: 'compressed'
//         }))
//         .pipe(prefixer('last 2 versions'))
//         .pipe(gulp.dest('dist/css'))
//         .pipe(browserSync.reload({stream: true}));
// });

gulp.task('styles', function () {
 return gulp.src('assets/scss/**/*.scss')
   .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
   .pipe(prefixer('last 2 versions'))
   .pipe(gulp.dest('assets/css'))
   .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', function(){
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    gulp.watch('assets/js/**/*.js', ['scripts']);
    gulp.watch('assets/scss/**/*.scss', ['styles']);
    gulp.watch('./**/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['scripts', 'styles', 'watch']);
